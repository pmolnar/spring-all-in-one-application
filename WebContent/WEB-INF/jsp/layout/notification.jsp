<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${notify!=null}">
	<h4 class="alert_${notify.level}">${notify.message}</h4>
	<c:remove var="notify"/>
</c:if>