<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

      <form class="form-signin" name="f" action='<c:url value="j_spring_security_check" />'>
        <h2 class="form-signin-heading">Please sign in</h2>
		<c:if test="${not empty error}">
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<h4>Your login attempt was not successful, try again.</h4>
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>
        <input type="text" name="j_username" class="input-block-level" placeholder="Email address">
        <input type="password" name="j_password" class="input-block-level" placeholder="Password">
        <!-- 
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
         -->
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>
