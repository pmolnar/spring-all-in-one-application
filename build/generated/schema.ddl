
    ;

    ;

    alter table CASH_FLOW 
        drop 
        foreign key FK_CF_TO_CATEGORY;

    alter table CASH_FLOW 
        drop 
        foreign key FKD4F346BA378039A4;

    alter table USER_BUDGETS 
        drop 
        foreign key FK_USER_TO_BUDGET;

    alter table USER_BUDGETS 
        drop 
        foreign key FK_BUDGET_TO_USER;

    drop table if exists ACCOUNT;

    drop table if exists BUDGET;

    drop table if exists CASH_FLOW;

    drop table if exists CATEGORY;

    drop table if exists USER_BUDGETS;

    drop table if exists hibernate_sequence;

    create table ACCOUNT (
        techID bigint not null,
        name varchar(255),
        login varchar(25),
        password varchar(255),
        email varchar(255),
        phone varchar(255),
        role integer not null,
        primary key (techID),
        unique (login)
    ) ENGINE=InnoDB;

    create table BUDGET (
        techID bigint not null,
        name varchar(255),
        min_limit double precision,
        summa double precision,
        primary key (techID)
    ) ENGINE=InnoDB;

    create table CASH_FLOW (
        techID bigint not null,
        description varchar(255),
        direction integer not null,
        amount double precision,
        catId bigint,
        BUDGET_ID bigint,
        primary key (techID)
    ) ENGINE=InnoDB;

    create table CATEGORY (
        techID bigint not null,
        name varchar(255),
        primary key (techID)
    ) ENGINE=InnoDB;

    create table USER_BUDGETS (
        BUDGET_ID bigint not null,
        USER_ID bigint not null,
        primary key (USER_ID, BUDGET_ID)
    ) ENGINE=InnoDB;

    alter table CASH_FLOW 
        add index FK_CF_TO_CATEGORY (catId), 
        add constraint FK_CF_TO_CATEGORY 
        foreign key (catId) 
        references CATEGORY (techID);

    alter table CASH_FLOW 
        add index FKD4F346BA378039A4 (BUDGET_ID), 
        add constraint FKD4F346BA378039A4 
        foreign key (BUDGET_ID) 
        references BUDGET (techID);

    alter table USER_BUDGETS 
        add index FK_USER_TO_BUDGET (USER_ID), 
        add constraint FK_USER_TO_BUDGET 
        foreign key (USER_ID) 
        references ACCOUNT (techID);

    alter table USER_BUDGETS 
        add index FK_BUDGET_TO_USER (BUDGET_ID), 
        add constraint FK_BUDGET_TO_USER 
        foreign key (BUDGET_ID) 
        references BUDGET (techID);

    create table hibernate_sequence (
         next_val bigint 
    );

    insert into hibernate_sequence values ( 20000 );

    insert into Category(techID, name) values(100, 'Rezsi' ),(101,'Szórakozás');

    insert into ACCOUNT(techID, name, login, password, email, role) values(1,'Administrator', 'admin', md5('password') , 'admin@localhost', '1' );
