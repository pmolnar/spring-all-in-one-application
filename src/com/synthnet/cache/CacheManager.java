package com.synthnet.cache;

public class CacheManager
{
	private static CacheHandler	cacheBuffer	= new CacheHandler( 100, CacheHandler.class.toString(), false );

	public static void registerCache( Object cacheID, CacheHandler handler )
	{
		cacheBuffer.putEntry( cacheID, handler );
	}

	public static CacheHandler getDynamicCache( Object cacheID )
	{
		return getCache( cacheID, -1, false );
	}

	public static CacheHandler getStaticCache( Object cacheID )
	{
		return getCache( cacheID, -1, true );
	}

	private static CacheHandler getCache( Object cacheID, int size, boolean isStatic )
	{
		CacheHandler handler = (CacheHandler) cacheBuffer.getEntry( cacheID );
		if( handler == null )
		{
			synchronized( cacheBuffer )
			{
				if( handler == null )
				{
					registerCache( cacheID, new CacheHandler( size, cacheID.toString(), isStatic ) );
				}
			}
		}
		return (CacheHandler) cacheBuffer.getEntry( cacheID );
	}

}
