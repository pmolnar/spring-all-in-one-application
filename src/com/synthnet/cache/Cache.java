package com.synthnet.cache;

import java.util.LinkedHashMap;

import com.synthnet.log.AppLogger;

class Cache extends LinkedHashMap
{
	private static final long	serialVersionUID	= -5900172720894319777L;
	private static String		DEFAULT_NAME		= "ANONYM";
	private static final int	DEFAULT_CACHE_SIZE	= 100;

	private int					cacheSize			= 0;
	private String				name				= "";
	private final boolean		isStatic			= false;

	Cache( int size )
	{
		this( size, DEFAULT_NAME + "_" + System.currentTimeMillis(), false );
	}

	Cache( int size, String name )
	{
		this( size, name, false );
	}

	Cache( int size, String name, boolean isStatic )
	{
		super( size <= 0 ? DEFAULT_CACHE_SIZE : size, 0.75f, !isStatic );
		this.cacheSize = size <= 0 ? DEFAULT_CACHE_SIZE : size;
		this.name = name;
		AppLogger.getInstance().logDebug( getClass(),
				"Create " + (isStatic ? "static" : "dynamic") + " cache instance of CacheHandler : {} :, {}!",
				this.getName(), cacheSize );
	}

	@Override
	protected boolean removeEldestEntry( java.util.Map.Entry eldest )
	{
		return !isStatic ? this.size() > cacheSize : false;
	}

	int getInitialSize()
	{
		return cacheSize;
	}

	public String getName()
	{
		return this.name;
	}

	public boolean isStatic()
	{
		return isStatic;
	}

}
