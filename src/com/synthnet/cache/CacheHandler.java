package com.synthnet.cache;

import com.synthnet.log.AppLogger;

public class CacheHandler
{
	private final Cache	cache;

	CacheHandler( int size, String name, boolean isStatic )
	{
		this.cache = new Cache( size, name, isStatic );
	}

	public void putEntry( Object key, Object value )
	{
		if( cache.isStatic() )
		{
			this._putEntryWithSynchronization( key, value );
		}
		else
		{
			this._putEntry( key, value );
		}
	}

	public void removeEntry( Object key )
	{
		if( cache.isStatic() )
		{
			this._removeEntryWithSynchronization( key );
		}
		else
		{
			this._removeEntry( key );
		}
	}

	public Object getEntry( Object key )
	{
		if( cache.isStatic() )
		{
			return this._getEntryWithSynchronization( key );
		}
		else
		{
			return this._getEntry( key );
		}
	}

	public boolean containsEntry( Object key )
	{
		if( cache.isStatic() )
		{
			return this._containsEntryWithSynchronization( key );
		}
		else
		{
			return this._containsEntry( key );
		}
	}

	@SuppressWarnings("unchecked")
	private void _putEntryWithSynchronization( Object key, Object value )
	{
		AppLogger.getInstance().logTrace( getClass(), "Synchronizing cache before insert: [{}] : {} => {}",
				cache.getName(), key, value );
		synchronized( this )
		{
			this._putEntry( key, value );
		}
	}

	@SuppressWarnings("unchecked")
	private void _putEntry( Object key, Object value )
	{
		long l = System.currentTimeMillis();
		cache.put( key, value );
		AppLogger.getInstance().logDebug( getClass(), "Put object into cache : {} : [ {} ]: {} took: {} ms",
				cache.getName(), key, value, (System.currentTimeMillis() - l) );
	}

	@SuppressWarnings("unchecked")
	private void _removeEntryWithSynchronization( Object key )
	{
		AppLogger.getInstance().logTrace( getClass(), "Synchronizing cache before remove: [{}] : {}", cache.getName(),
				key );
		synchronized( this )
		{
			this._removeEntry( key );
		}
	}

	@SuppressWarnings("unchecked")
	private void _removeEntry( Object key )
	{
		long l = System.currentTimeMillis();
		cache.put( key, null );
		AppLogger.getInstance().logDebug( getClass(), "Remove object from cache : {} : [ {} ]: took: {} ms",
				cache.getName(), key, (System.currentTimeMillis() - l) );
	}

	private Object _getEntryWithSynchronization( Object key )
	{
		AppLogger.getInstance().logTrace( getClass(), "Synchronizing cache before retrive entry: [{}] : {}",
				cache.getName(), key );
		synchronized( this )
		{
			return this._getEntry( key );
		}
	}

	private Object _getEntry( Object key )
	{
		long l = System.currentTimeMillis();
		Object o = cache.get( key );
		AppLogger.getInstance().logDebug( getClass(), "Searching for object in cache : {} : [ {} ]: took: {} ms",
				cache.getName(), key, (System.currentTimeMillis() - l) );
		if( o != null )
		{
			AppLogger.getInstance().logDebug( getClass(), "Retrieve object from cache : {} : [ {} ]: took: {} ms",
					cache.getName(), key, (System.currentTimeMillis() - l) );
		}
		return o;
	}

	private boolean _containsEntryWithSynchronization( Object key )
	{
		AppLogger.getInstance().logTrace( getClass(),
				"Synchronizing cache before checking availability of entry: [{}] : {}", cache.getName(), key );
		synchronized( this )
		{
			return this._containsEntry( key );
		}
	}

	private boolean _containsEntry( Object key )
	{
		long l = System.currentTimeMillis();
		synchronized( this )
		{
			AppLogger.getInstance().logDebug( getClass(), "Check existing in cache: {} : [ {} ]: took: {} ms",
					cache.getName(), key, (System.currentTimeMillis() - l) );
			return cache.containsKey( key );
		}
	}

	public void dumpCache()
	{

	}

	public void flush()
	{
		long l = System.currentTimeMillis();
		synchronized( this )
		{
			cache.clear();
			AppLogger.getInstance().logDebug(
					getClass(),
					"Remove all entries from cache :" + cache.getName() + ": took: " + (System.currentTimeMillis() - l)
							+ " ms" );
		}
	}
}
