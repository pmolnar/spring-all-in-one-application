package com.synthnet.log;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;

public class MethodLogger
{
	private ThreadLocal<Counter>	counter	= new ThreadLocal<MethodLogger.Counter>();

	private synchronized Counter getCounter()
	{
		if( counter.get() == null )
		{
			counter.set( new Counter() );
		}
		return counter.get();
	}

	public void methodBefore( final JoinPoint joinPoint )
	{
		AppLogger.getInstance().logDebug( getClass(), "{}STEP_IN [ {} -> {} ]: {}", getCounter().inc(), joinPoint.getTarget(),
				joinPoint.getSignature().getName(), joinPoint.getSignature() );
	}

	public void methodAfter( final JoinPoint joinPoint )
	{
		AppLogger.getInstance().logDebug( getClass(), "{}STEP_OUT [ {} -> {} ]: {}", getCounter().dec(), joinPoint.getTarget(),
				joinPoint.getSignature().getName(), joinPoint.getSignature(), System.currentTimeMillis() );
	}

	public void methodAfterReturn( final JoinPoint joinPoint )
	{
		//		AppLogger.getInstance().stepOutMethod( getClass(), joinPoint.getSignature().getName() + "Return" );
	}

	public void methodAfterThrowing( final JoinPoint joinPoint )
	{
		//		AppLogger.getInstance().stepOutMethod( getClass(), joinPoint.getSignature().getName() + "Throwing" );
	}

	private class Counter
	{
		private int	value	= 0;

		public String inc()
		{
			return StringUtils.leftPad( "", value++, "\t" );
		}

		public String dec()
		{
			return StringUtils.leftPad( "", --value, "\t" );
		}
	}

}
