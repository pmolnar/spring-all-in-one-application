package com.synthnet.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class LogFactory
{
	static Logger getLogger( String className )
	{
		return LoggerFactory.getLogger( className );
	}

	static Logger getLogger( Class clazz )
	{
		return LoggerFactory.getLogger( clazz );
	}

}
