package com.synthnet.log;

import org.slf4j.Logger;

public class AppLogger
{
	private static class InstanceHolder
	{
		protected static final AppLogger	INSTANCE	= new AppLogger();
	}

	private AppLogger()
	{
	}

	public static synchronized AppLogger getInstance()
	{
		return InstanceHolder.INSTANCE;
	}

	public void logError( Class clazz, String method, Throwable t, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		if( log.isErrorEnabled() )
		{
			synchronized( log )
			{
				log.error( message + " Ex: " + t.getMessage(), objects );
				if( t != null )
				{
					StackTraceElement[] stack = t.getStackTrace();
					for( StackTraceElement stackTraceElement : stack )
					{
						log.error( stackTraceElement.toString() );
					}
				}
			}
		}
	}

	public void logError( Class clazz, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		log.error( message, objects );
	}

	public void logWarning( Class clazz, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		log.warn( message, objects );
	}

	public void logInfo( Class clazz, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		log.info( message, objects );
	}

	public void logDebug( Class clazz, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		log.debug( message, objects );
	}

	public void logTrace( Class clazz, String message, Object... objects )
	{
		Logger log = LogFactory.getLogger( clazz.getName() );
		log.trace( message, objects );
	}
}
