package com.synthnet.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.synthnet.log.AppLogger;

public class ConfigReader
{
	//	private CombinedConfiguration configuration = new DefaultConfigurationBuilder("conguration.xml").getConfiguration( true );

	private static Properties	props			= new Properties();
	private static final String	PROPFILE_PATH	= "springdemo.properties";

	static
	{
		//InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream( PROPFILE_PATH );
		InputStream is = ConfigReader.class.getClassLoader().getResourceAsStream( PROPFILE_PATH );
		if( is != null )
		{
			try
			{
				props.load( is );
			}
			catch( IOException e )
			{
				AppLogger.getInstance().logError( ConfigReader.class, "[static]", e,
						"Couldn't initialize from properties file ( " + PROPFILE_PATH + " )." );
			}
		}
	}

	public static final String getProperty( String key )
	{

		String res = System.getProperty( key );
		if( (res == null || "".equals( res.trim() )) && props != null )
		{
			res = props.getProperty( key );
		}
		return res;
	}

	public static String getProperty( String key, String def )
	{

		String res = getProperty( key );
		if( res == null || "".equals( res.trim() ) )
		{
			AppLogger.getInstance().logDebug(
					ConfigReader.class,
					"Didn't find value for key: '" + key + "' ( " + PROPFILE_PATH + " ), use deafult value: '" + def
							+ "'." );
		}

		return res == null || "".equals( res.trim() ) ? def : res;
	}

	public static final long getLongProperty( String key, long def )
	{
		String tmp = getProperty( key, def + "" );
		try
		{
			return Long.parseLong( tmp );
		}
		catch( NumberFormatException e )
		{
			AppLogger.getInstance().logError( AppLogger.class, "getLongProperty", e,
					"Invalid value for key: {}, return value is: ", tmp, def );
			return def;
		}
	}

}
