package com.synthnet.common;

public class HostInfo
{
	private static class InstanceHolder
	{
		protected static final HostInfo	INSTANCE	= new HostInfo();
	}

	private HostInfo()
	{
	}

	public static synchronized HostInfo getInstance()
	{
		return InstanceHolder.INSTANCE;
	}

	private String	applicationHost	= null;

	public String getApplicationHost()
	{
		return applicationHost;
	}

	public void setApplicationHost( String applicationHost )
	{
		this.applicationHost = applicationHost;
	}

}
