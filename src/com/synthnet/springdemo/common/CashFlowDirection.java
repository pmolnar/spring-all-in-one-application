package com.synthnet.springdemo.common;

public enum CashFlowDirection {
	DIRECTION_INCOME, DIRECTION_OUTLAY;
}
