package com.synthnet.springdemo.common;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority
{
	ROLE_DEMO_ADMIN, ROLE_DEMO_USER,

	;

	public String getAuthority()
	{
		return toString();
	}
}
