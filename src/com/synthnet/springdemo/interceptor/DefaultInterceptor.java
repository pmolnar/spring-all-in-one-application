package com.synthnet.springdemo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.synthnet.common.HostInfo;
import com.synthnet.log.AppLogger;

public class DefaultInterceptor extends HandlerInterceptorAdapter
{

	@Override
	public void afterCompletion( HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3 )
			throws Exception
	{
		AppLogger.getInstance().logDebug( getClass(), "afterCompletion: {} - URI: {}, Exception: {}", arg2,
				arg0.getRequestURI(), arg3 == null ? "NO_EXCEPTION" : arg3 );
	}

	@Override
	public void postHandle( HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3 )
			throws Exception
	{
		AppLogger.getInstance().logDebug( getClass(), "postHandle:{}, {}", arg2, arg3 );
	}

	@Override
	public boolean preHandle( HttpServletRequest arg0, HttpServletResponse arg1, Object arg2 ) throws Exception
	{
		StringBuffer sb = new StringBuffer();
		sb.append( arg0.getScheme() ).append( "://" ).append( arg0.getServerName() ).append( ":" )
				.append( arg0.getServerPort() ).append( arg0.getContextPath() );
		HostInfo.getInstance().setApplicationHost( sb.toString() );
		return true;
	}

}
