package com.synthnet.springdemo.service.impl;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import com.synthnet.springdemo.service.IBudgetService;
import com.synthnet.springdemo.service.ICashFlowService;
import com.synthnet.springdemo.service.ICategoryService;
import com.synthnet.springdemo.service.IServices;
import com.synthnet.springdemo.service.IUserService;

public class ServiceFactory implements IServices
{
	private static class InstanceHolder
	{
		protected static final IServices	INSTANCE	= new ServiceFactory();
	}

	private ServiceFactory()
	{
	}

	public static synchronized IServices getInstance()
	{
		return InstanceHolder.INSTANCE;
	}

	@Autowired
	private IUserService		userService;
	@Autowired
	private IBudgetService		budgetService;
	@Autowired
	private ICashFlowService	cashFlowService;
	@Autowired
	private ICategoryService	categoryService;

	@Autowired
	private MessageSource		messageSource;

	public String getMessage( String code, Object[] args )
	{
		try
		{
			return messageSource.getMessage( code, args, code, Locale.getDefault() );
		}
		catch( NoSuchMessageException e )
		{
			StringBuffer sb = new StringBuffer();
			sb.append( code );
			if( args != null )
			{
				sb.append( "{ " );
				for( int i = 0; i < args.length; i++ )
				{
					Object object = args[i];
					sb.append( object );
					if( i + 1 < args.length )
					{
						sb.append( ", " );
					}
				}
				sb.append( " }" );
			}
			return sb.toString();
		}
	}

	public String getMessage( String code )
	{
		return getMessage( code, null );
	}

	public IUserService getUserService()
	{
		return userService;
	}

	public IBudgetService getBudgetService()
	{
		return budgetService;
	}

	public ICashFlowService getCashFlowService()
	{
		return cashFlowService;
	}

	public ICategoryService getCategoryService()
	{
		return categoryService;
	}

}
