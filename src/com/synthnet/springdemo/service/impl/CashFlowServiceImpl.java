package com.synthnet.springdemo.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.springdemo.bean.pojo.CashFlow;
import com.synthnet.springdemo.persistence.dao.ICashFlowDAO;
import com.synthnet.springdemo.service.ICashFlowService;

@Service
@Transactional
@PreAuthorize(value = "isAuthenticated()")
class CashFlowServiceImpl extends BaseCrudService<ICashFlowDAO, CashFlow, Long> implements ICashFlowService
{

}
