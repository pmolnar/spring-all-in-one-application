package com.synthnet.springdemo.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.springdemo.bean.pojo.Category;
import com.synthnet.springdemo.persistence.dao.ICategoryDAO;
import com.synthnet.springdemo.service.ICategoryService;

@Service
@Transactional
@PreAuthorize(value = "isAuthenticated()")
class CategoryServiceImpl extends BaseCrudService<ICategoryDAO, Category, Long> implements ICategoryService
{

}
