package com.synthnet.springdemo.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.springdemo.bean.pojo.Budget;
import com.synthnet.springdemo.persistence.dao.IBudgetDAO;
import com.synthnet.springdemo.service.IBudgetService;

@Service
@Transactional
@PreAuthorize(value = "isAuthenticated()")
class BudgetServiceImpl extends BaseCrudService<IBudgetDAO, Budget, Long> implements IBudgetService
{

}
