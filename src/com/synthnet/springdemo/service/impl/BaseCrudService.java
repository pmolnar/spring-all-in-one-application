package com.synthnet.springdemo.service.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.cache.CacheHandler;
import com.synthnet.cache.CacheManager;
import com.synthnet.springdemo.bean.Entity;
import com.synthnet.springdemo.bean.pojo.Budget;
import com.synthnet.springdemo.bean.pojo.CashFlow;
import com.synthnet.springdemo.bean.pojo.Category;
import com.synthnet.springdemo.bean.pojo.User;
import com.synthnet.springdemo.persistence.dao.IBudgetDAO;
import com.synthnet.springdemo.persistence.dao.ICashFlowDAO;
import com.synthnet.springdemo.persistence.dao.ICategoryDAO;
import com.synthnet.springdemo.persistence.dao.IDAO;
import com.synthnet.springdemo.persistence.dao.IUserDAO;
import com.synthnet.springdemo.service.ICrudService;

abstract class BaseCrudService<DomainDAO extends IDAO<DomainObject, Key>, DomainObject extends Entity, Key extends Serializable>
		extends AbstractDAOHelper<DomainDAO, DomainObject, Key> implements ICrudService<DomainObject, Key>,
		ApplicationContextAware
{

	static CacheHandler	cache		= CacheManager.getStaticCache( "CRUDSERVICE:DAOS" );

	Class				domainClass	= getDomainClass();

	static
	{
		cache.putEntry( User.class, IUserDAO.class );
		cache.putEntry( Budget.class, IBudgetDAO.class );
		cache.putEntry( Category.class, ICategoryDAO.class );
		cache.putEntry( CashFlow.class, ICashFlowDAO.class );
	}

	@SuppressWarnings("unchecked")
	protected Class<DomainObject> getDomainClass()
	{
		if( domainClass == null )
		{
			ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
			domainClass = (Class<DomainObject>) thisType.getActualTypeArguments()[1];
		}
		return domainClass;
	}

	@Transactional(readOnly = false)
	public void save( DomainObject object )
	{
		getDAO().saveOrUpdate( object );
	}

	@Transactional(readOnly = false)
	public void delete( DomainObject object )
	{
		getDAO().delete( object );
	}

	public void delete( Key key )
	{
		getDAO().deleteById( key );
	}

	public DomainObject update( DomainObject object )
	{
		return getDAO().merge( object );
	}

	public DomainObject load( Key object )
	{
		return getDAO().load( object );
	}

	public List<DomainObject> loadAll()
	{
		return getDAO().getList();
	}

	public List<DomainObject> findByProperty( String property, Object value )
	{
		return getDAO().findByProperty( property, value );
	}

	public List<DomainObject> findByJunction( Junction junction )
	{
		return getDAO().findByJunction( junction );
	}

	public List<DomainObject> findByJunction( Junction junction, Order order )
	{
		return getDAO().findByJunction( junction );
	}

	public void setApplicationContext( ApplicationContext apctx ) throws BeansException
	{
		Class dao = (Class) cache.getEntry( domainClass );
		if( dao == null )
		{
			throw new RuntimeException( "You should register dao for entity: " + domainClass );
		}
		this.setDAO( (DomainDAO) apctx.getBean( dao ) );
	}

	@SuppressWarnings(
	{ "unchecked" })
	protected <T> T getTargetObject( Object proxy, Class<T> targetClass ) throws Exception
	{
		if( AopUtils.isJdkDynamicProxy( proxy ) )
		{
			return (T) ((Advised) proxy).getTargetSource().getTarget();
		}
		else
		{
			return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
		}
	}

}

abstract class AbstractDAOHelper<DomainDAO extends IDAO<DomainObject, KeyType>, DomainObject extends Serializable, KeyType extends Serializable>
{
	private IDAO<DomainObject, KeyType>	dao;

	protected DomainDAO getDAO()
	{
		return (DomainDAO) dao;
	}

	protected void setDAO( DomainDAO impl )
	{
		dao = impl;
	}

}
