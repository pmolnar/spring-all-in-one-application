package com.synthnet.springdemo.service.impl;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.log.AppLogger;
import com.synthnet.springdemo.bean.pojo.User;
import com.synthnet.springdemo.persistence.dao.IUserDAO;
import com.synthnet.springdemo.service.IUserService;

@Service(value = "userService")
@Transactional
@PreAuthorize(value = "isAuthenticated()")
class UserServiceImpl extends BaseCrudService<IUserDAO, User, Long> implements IUserService
{

	public UserDetails loadUserByUsername( String arg0 ) throws UsernameNotFoundException
	{
		List<User> users = getDAO().findByProperty( "login", arg0 );
		if( users == null || users.size() != 1 )
		{
			AppLogger.getInstance().logWarning( getClass(), "User login failed: {}", arg0 );
			throw new UsernameNotFoundException( "User login failed!" );
		}

		return users.get( 0 );
	}

	public User getAuthenticatedUser()
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if( authentication != null && authentication.getPrincipal() != null
				&& authentication.getPrincipal() instanceof User )
		{
			return (User) authentication.getPrincipal();

		}
		return null;
	}

}
