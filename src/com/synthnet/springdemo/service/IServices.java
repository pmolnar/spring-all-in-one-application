package com.synthnet.springdemo.service;

public interface IServices
{
	public IUserService getUserService();

	public IBudgetService getBudgetService();

	public ICashFlowService getCashFlowService();

	public ICategoryService getCategoryService();

	public String getMessage( String code, Object[] args );

	public String getMessage( String code );

}
