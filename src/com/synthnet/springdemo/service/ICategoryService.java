package com.synthnet.springdemo.service;

import com.synthnet.springdemo.bean.pojo.Category;

public interface ICategoryService extends ICrudService<Category, Long>
{

}
