package com.synthnet.springdemo.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.synthnet.springdemo.bean.pojo.User;

public interface IUserService extends ICrudService<User, Long>, UserDetailsService
{

	User getAuthenticatedUser();

}
