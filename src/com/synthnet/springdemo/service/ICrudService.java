package com.synthnet.springdemo.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;

import com.synthnet.springdemo.bean.Entity;

public interface ICrudService<DomainObject extends Entity, Key extends Serializable>
{
	public void save( DomainObject object );

	public void delete( DomainObject object );

	public void delete( Key key );

	public DomainObject update( DomainObject object );

	public DomainObject load( Key object );

	List<DomainObject> loadAll();

	List<DomainObject> findByProperty( String property, Object value );

	List<DomainObject> findByJunction( Junction junction, Order order );

}
