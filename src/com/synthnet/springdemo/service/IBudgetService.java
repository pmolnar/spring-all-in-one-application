package com.synthnet.springdemo.service;

import com.synthnet.springdemo.bean.pojo.Budget;

public interface IBudgetService extends ICrudService<Budget, Long>
{

}
