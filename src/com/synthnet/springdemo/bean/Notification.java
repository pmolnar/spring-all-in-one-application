package com.synthnet.springdemo.bean;

public class Notification
{
	public static final String	NOTIF_INFO		= "info";
	public static final String	NOTIF_SUCCESS	= "success";
	public static final String	NOTIF_ERROR		= "error";
	public static final String	NOTIF_WARNING	= "warning";

	private String				level;
	private String				message;

	private StackTraceElement[]	elements;

	// Factory methods

	public static Notification createSuccess( String message )
	{
		return new Notification( NOTIF_SUCCESS, message );
	}

	public static Notification createInfo( String message )
	{
		return new Notification( NOTIF_INFO, message );
	}

	public static Notification createWarning( String message )
	{
		return new Notification( NOTIF_WARNING, message );
	}

	public static Notification createError( String message, Exception e )
	{
		return new Notification( message, e );
	}

	//Constructors

	protected Notification()
	{
		super();
	}

	private Notification( String level, String message )
	{
		this();
		this.level = level;
		this.message = message;
	}

	private Notification( String message, Exception e )
	{
		this();
		this.level = NOTIF_ERROR;
		if( e != null )
		{
			String lmessage = message != null ? message : "";
			this.message = e.getMessage() + lmessage;
			this.elements = e.getStackTrace();
		}
		else
		{
			this.message = message;
		}
	}

	public String getLevel()
	{
		return level;
	}

	public void setLevel( String level )
	{
		this.level = level;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage( String message )
	{
		this.message = message;
	}

	public StackTraceElement[] getElements()
	{
		return elements;
	}

	public void setElements( StackTraceElement[] elements )
	{
		this.elements = elements;
	}

}
