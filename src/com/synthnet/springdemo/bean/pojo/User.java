package com.synthnet.springdemo.bean.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.synthnet.springdemo.common.Role;

public class User extends AbstractUser implements UserDetails
{

	public User()
	{
		super();
	}

	public User( Role role )
	{
		super( role );
	}

	public User( String name, String login, String password, String email, String phone, Role role, Set<Budget> budgets )
	{
		super( name, login, password, email, phone, role, budgets );
	}

	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add( this.getRole() );
		return authorities;
	}

	public String getUsername()
	{
		return this.getLogin();
	}

	public boolean isAccountNonExpired()
	{
		return true;
	}

	public boolean isAccountNonLocked()
	{
		return true;
	}

	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	public boolean isEnabled()
	{
		return true;
	}

}
