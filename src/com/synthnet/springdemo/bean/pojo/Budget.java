package com.synthnet.springdemo.bean.pojo;

import java.util.Set;

public class Budget extends AbstractBudget
{

	public Budget()
	{
		super();
	}

	public Budget( String name, double min_limit, double summa, Set<CashFlow> transactions, Set<User> users )
	{
		super( name, min_limit, summa, transactions, users );
	}

}
