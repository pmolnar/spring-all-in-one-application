package com.synthnet.springdemo.bean.pojo;

import com.synthnet.springdemo.common.CashFlowDirection;

public class CashFlow extends AbstractCashFlow
{

	public CashFlow()
	{
		super();
	}

	public CashFlow( CashFlowDirection direction )
	{
		super( direction );
	}

	public CashFlow( String description, CashFlowDirection direction, double amount, Category category )
	{
		super( description, direction, amount, category );
	}

}