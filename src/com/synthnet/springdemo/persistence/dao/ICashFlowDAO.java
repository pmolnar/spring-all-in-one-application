package com.synthnet.springdemo.persistence.dao;

import com.synthnet.springdemo.bean.pojo.CashFlow;

public interface ICashFlowDAO extends IDAO<CashFlow, Long>
{

}
