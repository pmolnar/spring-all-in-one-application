package com.synthnet.springdemo.persistence.dao.impl;

import java.io.Serializable;

abstract class BaseHibernateVIEWDAOImpl<VIEW extends Serializable, KEY extends Serializable> extends
		BaseHibernateDAOImpl<VIEW, KEY>
{

	@Override
	final public void delete( VIEW object )
	{
		throwMethodNotSupported();
	}

	@Override
	final public void deleteAll()
	{
		throwMethodNotSupported();
	}

	@Override
	final public void deleteById( KEY id )
	{
		throwMethodNotSupported();
	}

	@Override
	final public void save( VIEW object )
	{
		throwMethodNotSupported();
	}

	@Override
	final public void update( VIEW object )
	{
		throwMethodNotSupported();
	}

	@Override
	final public VIEW merge( VIEW object )
	{
		throwMethodNotSupported();
		return null;
	}

	@Override
	final public void saveOrUpdate( VIEW object )
	{
		throwMethodNotSupported();
	}

	private void throwMethodNotSupported()
	{
		throw new RuntimeException( "You cannot call this method on view!" );
	}

}
