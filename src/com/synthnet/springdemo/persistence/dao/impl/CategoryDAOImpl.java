package com.synthnet.springdemo.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.synthnet.springdemo.bean.pojo.Category;
import com.synthnet.springdemo.persistence.dao.ICategoryDAO;

@Repository
public class CategoryDAOImpl extends BaseHibernateDAOImpl<Category, Long> implements ICategoryDAO
{

	@Override
	protected void init()
	{
	}

}
