package com.synthnet.springdemo.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.synthnet.springdemo.bean.pojo.CashFlow;
import com.synthnet.springdemo.persistence.dao.ICashFlowDAO;

@Repository
public class CashFlowDAOImpl extends BaseHibernateDAOImpl<CashFlow, Long> implements ICashFlowDAO
{

	@Override
	protected void init()
	{
	}

}
