package com.synthnet.springdemo.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.synthnet.springdemo.bean.pojo.Budget;
import com.synthnet.springdemo.persistence.dao.IBudgetDAO;

@Repository
public class BudgetDAOImpl extends BaseHibernateDAOImpl<Budget, Long> implements IBudgetDAO
{

	@Override
	protected void init()
	{
	}

}
