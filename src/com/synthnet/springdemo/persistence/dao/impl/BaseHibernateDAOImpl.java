package com.synthnet.springdemo.persistence.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.synthnet.springdemo.persistence.dao.IDAO;

abstract class BaseHibernateDAOImpl<DomainObject extends Serializable, KeyType extends Serializable> implements
		IDAO<DomainObject, KeyType>
{
	protected Class<DomainObject>	domainClass	= getDomainClass();

	@Autowired
	private SessionFactory			sessionFactory;

	public Session getSession()
	{
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	protected Class<DomainObject> getDomainClass()
	{
		if( domainClass == null )
		{
			ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
			domainClass = (Class<DomainObject>) thisType.getActualTypeArguments()[0];
		}
		return domainClass;
	}

	abstract protected void init();

	public long count()
	{
		return ((Long) getSession().createCriteria( domainClass ).setProjection( Projections.rowCount() ).list()
				.get( 0 )).longValue();
	}

	public long countByProperties( Map params )
	{
		if( params == null )
		{
			return count();
		}
		Criteria criteria = getSession().createCriteria( domainClass );
		criteria.add( Restrictions.allEq( params ) );
		criteria.setProjection( Projections.rowCount() );
		List count = criteria.list();

		return (Long) (count == null || count.size() != 1 ? 0l : count.get( 0 ));
	}

	public void delete( DomainObject object )
	{
		getSession().delete( object );
	}

	public void deleteAll()
	{
		throw new RuntimeException( "deleteAll operation not allowed!" );
	}

	public void deleteById( KeyType id )
	{
		delete( load( id ) );
	}

	public void detach( DomainObject object )
	{
		getSession().evict( object );
	}

	@SuppressWarnings("unchecked")
	public List<DomainObject> getList()
	{
		return getSession().createCriteria( domainClass ).list();
	}

	@SuppressWarnings("unchecked")
	public DomainObject load( KeyType id )
	{
		DomainObject d = (DomainObject) getSession().get( domainClass, id );
		return d;
	}

	@SuppressWarnings("unchecked")
	public List<DomainObject> loadByExample( DomainObject object )
	{
		return getSession().createCriteria( domainClass ).add( Example.create( object ) ).list();
	}

	@Transactional(readOnly = false)
	public void save( DomainObject object )
	{
		getSession().save( object );
	}

	public void update( DomainObject object )
	{
		getSession().update( object );
	}

	@SuppressWarnings("unchecked")
	public DomainObject merge( DomainObject object )
	{
		return (DomainObject) getSession().merge( object );
	}

	@Transactional(readOnly = false)
	public void saveOrUpdate( DomainObject object )
	{
		getSession().saveOrUpdate( object );
	}

	public List<DomainObject> findByOrderedJunctionList( Junction junction )
	{
		if( junction == null )
		{
			return getList();
		}
		Criteria criteria = getSession().createCriteria( domainClass );
		criteria.add( junction );
		return criteria.list();
	}

	public List<DomainObject> findByProperty( String property, Object value )
	{
		return findByJunction( Restrictions.conjunction().add( Restrictions.eq( property, value ) ) );
	}

	public List<DomainObject> findByJunction( Junction junction )
	{
		Criteria criteria = getSession().createCriteria( domainClass );
		criteria.add( junction );
		return criteria.list();
	}

	public List<DomainObject> findByJunction( Junction junction, Order order )
	{
		Criteria criteria = getSession().createCriteria( domainClass );
		criteria.add( junction );
		criteria.addOrder( order );
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<DomainObject> findByNamedQuery( String namedQuery, Map<String, Object> params, Integer... maxResult )
	{
		Query query = getSession().getNamedQuery( namedQuery );
		if( params != null )
		{
			Iterator<String> keys = params.keySet().iterator();
			while( keys.hasNext() )
			{
				String key = keys.next();
				Object value = params.get( key );
				if( value instanceof Collection )
				{
					query.setParameterList( key, (Collection) value );
				}
				else
				{
					query.setParameter( key, value );
				}
			}
		}
		if( maxResult != null && maxResult.length > 0 )
		{
			query.setMaxResults( (maxResult[0]) );
		}
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List findBySQLNamedQuery( String namedQuery, Map<String, Object> params, Class resultClass,
			Integer... maxResult )
	{
		SQLQuery query = (SQLQuery) getSession().getNamedQuery( namedQuery );
		if( params != null )
		{
			Iterator<String> keys = params.keySet().iterator();
			while( keys.hasNext() )
			{
				String key = keys.next();
				Object value = params.get( key );
				if( value instanceof Collection )
				{
					query.setParameterList( key, (Collection) value );
				}
				else
				{
					query.setParameter( key, value );
				}
			}
		}
		if( maxResult != null && maxResult.length > 0 )
		{
			query.setMaxResults( (maxResult[0]) );
		}
		if( resultClass != null )
		{
			query.addEntity( resultClass );
		}
		return query.list();
	}

}