package com.synthnet.springdemo.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.synthnet.springdemo.bean.pojo.User;
import com.synthnet.springdemo.persistence.dao.IUserDAO;

@Repository
public class UserDAOImpl extends BaseHibernateDAOImpl<User, Long> implements IUserDAO
{

	@Override
	protected void init()
	{
	}

}
