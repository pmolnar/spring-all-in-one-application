package com.synthnet.springdemo.persistence.dao;

import com.synthnet.springdemo.bean.pojo.Category;

public interface ICategoryDAO extends IDAO<Category, Long>
{

}
