package com.synthnet.springdemo.persistence.dao;

import com.synthnet.springdemo.bean.pojo.Budget;

public interface IBudgetDAO extends IDAO<Budget, Long>
{

}
