package com.synthnet.springdemo.persistence.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;

public interface IDAO<DomainObject extends Serializable, KeyType extends Serializable>
{
	public DomainObject load( KeyType id );

	public List<DomainObject> loadByExample( DomainObject object );

	public void update( DomainObject object );

	public void save( DomainObject object );

	public void saveOrUpdate( DomainObject object );

	public void detach( DomainObject object );

	public DomainObject merge( DomainObject object );

	public void delete( DomainObject object );

	public void deleteById( KeyType id );

	public List<DomainObject> getList();

	public void deleteAll();

	public long count();

	public long countByProperties( Map params );

	public List<DomainObject> findByJunction( Junction junction );

	public List<DomainObject> findByJunction( Junction junction, Order order );

	public List<DomainObject> findByProperty( String property, Object value );

	public List<DomainObject> findByNamedQuery( String namedQuery, Map<String, Object> params, Integer... maxResult );

	public List findBySQLNamedQuery( String namedQuery, Map<String, Object> params, Class resultClass,
			Integer... maxResult );

}