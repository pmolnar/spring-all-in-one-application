package com.synthnet.springdemo.persistence.dao;

import com.synthnet.springdemo.bean.pojo.User;

public interface IUserDAO extends IDAO<User, Long>
{

}
