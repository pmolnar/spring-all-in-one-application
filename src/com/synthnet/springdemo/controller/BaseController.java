package com.synthnet.springdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.synthnet.springdemo.bean.Notification;

@Controller
public class BaseController
{
	public static final String	NOTIFICATION_INFO	= "notify";
	public static final String	CALLBACK_METHOD		= "callback";
	public static final String	FORCEDWEB			= "forcedweb";

	protected void addNotification( Notification notification )
	{
		RequestContextHolder.currentRequestAttributes().setAttribute( BaseController.NOTIFICATION_INFO, notification,
				RequestAttributes.SCOPE_SESSION );
	}

	protected void info( String message )
	{
		RequestContextHolder.currentRequestAttributes().setAttribute( BaseController.NOTIFICATION_INFO,
				Notification.createInfo( message ), RequestAttributes.SCOPE_SESSION );
	}

	protected void success( String message )
	{

		RequestContextHolder.currentRequestAttributes().setAttribute( BaseController.NOTIFICATION_INFO,
				Notification.createSuccess( message ), RequestAttributes.SCOPE_SESSION );
	}

	protected void errorNotify( String message )
	{

		RequestContextHolder.currentRequestAttributes().setAttribute( BaseController.NOTIFICATION_INFO,
				Notification.createError( message, null ), RequestAttributes.SCOPE_SESSION );
	}

	protected void warning( String message )
	{
		RequestContextHolder.currentRequestAttributes().setAttribute( BaseController.NOTIFICATION_INFO,
				Notification.createWarning( message ), RequestAttributes.SCOPE_SESSION );
	}

	protected void setCallbackMethod( RequestMethod requestMethod, ModelAndView view )
	{
		view.addObject( CALLBACK_METHOD, requestMethod.toString() );
	}

	//	@org.springframework.web.bind.annotation.ExceptionHandler(value = Throwable.class)
	//	public void handleExceptions( final Throwable t, HttpServletRequest request, HttpServletResponse response )
	//			throws IOException
	//	{
	//		return;
	//	}

}
