package com.synthnet.springdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.synthnet.springdemo.service.impl.ServiceFactory;

@Controller
@RequestMapping(value = "/index")
public class DefaultController
{
	@RequestMapping
	public String index( ModelMap model )
	{
		model.addAttribute( "username", ServiceFactory.getInstance().getUserService().getAuthenticatedUser().getName() );
		return "test.content";
	}
}
